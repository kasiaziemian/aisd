import random 

class Node():
    def __init__(self, data=None):
        self.data = data
        self.left = left 
        self.right = right

class BinaryTree():
    def __init__(self):
        self.root = Node()
    
    def add(self, data):
        if self.root.data == None:
            self.root.data = data
        else:
            def add_to_vertex(vertex, data):
                if data < vertex.data:
                    if vertex.left == None:
                        vertex.left = Node(data)
                    else:
                        add_to_vertex(vertex.left. data)
                elif data > vertex.data:
                    if vertex.right == None:
                        vertex.right = Node(data)
                    else:
                        add_to_vertex(vertex.right, data)
            add_to_vertex(self.root, data)
    #vlr - wzdluzne
    #lvr - poprzeczne
    #lrv - wsteczne
    def display(self):
        output = ''
        def vlr(output, vertex):
            if vertex:
                output += (str(vertex.data) + '-')
                output = vlr(output, vertex.left) 
                output = vlr(output, vertex.right)
            return output
        
        def lvr(output, vertex):
            if vertex:
                output = vlr(output, vertex.left)
                output = (str(vertex.data) + '-')
                output = vlr(output, vertex.right)
            return output
        def lrv(output, vertex):
            if vertex:
                output = vlr(output, vertex.left)
                output = vlr(output, vertex.right)
                output = (str(vertex.data) + '-')
            return output
        
        print('VLR: ')
        print(vlr(output, self.root))
        print('LVR: ')
        print(lvr(output, self.root))
        print('LRV: ')
        print(lrv(output, self.root))
    
    def search(self, data):
        if self.root.data == data:
            return self.root
        def search_vertex(vertex, data):
            if vertex:
                if vertex.data == data:
                    return vertex
                elif data < vertex.data:
                    return search_vertex(vertex.left, data)
                else:
                    return search_vertex(vertex.right, data)
        return search_vertex(self.root, data)

b = BinaryTree()
for i in range(100):
    b.add(random.randint(0, 100))
b.add(5)
b.display()
print('Found')
for i in range(100):
    v = b.search(random.randint(0, 100))
    if v:
        print(v.data)
    else:
        print('None')