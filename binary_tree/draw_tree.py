import random 
import math
import os
import tkinter as tk
from tkinter import *
from tkinter import messagebox, simpledialog
import pygame
from pygame.locals import *

class Node():
    def __init__(self, data = None):
        self.data = data
        self.left = None 
        self.right = None

class BinaryTree():
    def __init__(self):
        self.root = Node()
        self.lenght = 0
    
    def add(self, data):
        if self.root.data == None:
            self.root.data = data
        else:
            def add_to_vertex(vertex, data):
                if data < vertex.data:
                    if vertex.left == None:
                        vertex.left = Node(data)
                    else:
                        add_to_vertex(vertex.left, data)
                elif data > vertex.data:
                    if vertex.right == None:
                        vertex.right = Node(data)
                    else:
                        add_to_vertex(vertex.right, data)
            add_to_vertex(self.root, data)
        self.lenght += 1
    
    def remove(self, data):
        node = self.search(data)
        if node:
            parent = self.search(self.find_parent(data))
            if self.wich_child(data) == 'right':
                if node.right:
                    parent.right = node.right
                    if node.left:
                        parent.right.left = node.left
                else:
                    parent.right = None
                    if node.left:
                        parent.right = node.left
            else:
                if node.left:
                    parent.left = node.left
                    if node.right:
                        node.left.right = node.right
                else:
                    parent.left = None
                    if node.right:
                        parent.left = node.right
    
    def display(self):
        result = ''
        def vlr(result, vertex):
            if vertex:
                result += (str(vertex.data) + '-')
                result = vlr(result, vertex.left)
                result = vlr(result, vertex.right)
            return result
        def lvr(result, vertex):
            if vertex:
                result = vlr(result, vertex.left)
                result += (str(vertex.data) + '-')
                result = vlr(result, vertex.right)
            return result
        
        def lrv(result, vertex):
            if vertex:
                result = vlr(result, vertex.left)
                result = vlr(result, vertex.right)
                result += (str(vertex.data) + '-')
            return result
        
        print('VLR: ')
        print(vlr(result, self.root))
        print('LVR: ')
        print(lvr(result, self.root))
        print('LRV: ')
        print(lrv(result, self.root))
    
    def if_root(self, y):
        if self.root.data == y:
            return True
        return False
    
    def find_level(self, y):
        return len(self.path(y)) - 1
    
    def find_parent(self, y):
        return self.path(y)[-2]
    
    def wich_child(self, y):
        if self.search(self.find_parent(y)).left:
            if self.search(self.find_parent(y)).left.data == y:
                return 'left'
        return 'right'
    
    def path(self, y):
        def _path(vertex, y, d=[]):
            if not vertex:
                return []
            if vertex.data == y:
                return [vertex.data]
            res = _path(vertex.left, y)
            if res:
                return [vertex.data] + res
            res = _path(vertex.right, y)
            if res:
                return [vertex.data] + res
            return []
        return _path(self.root, y)
    
    def vlr(self):
        def _vlr(vertex):
            data = []
            if vertex:
                data = _vlr(vertex.left)
                data.append(vertex.data)
                data += _vlr(vertex.right)
            return data
        return _vlr(self.root)
    
    def lvr(self):
        def _lvr(vertex):
            data = []
            if vertex:
                data.append(vertex.data)
                data += _lvr(vertex.left)
                data += _lvr(vertex.right)
            return data
        return _lvr(self.root)
    
    def lrv(self):
        def _lrv(vertex):
            data = []
            if vertex:
                data = _lrv(vertex.left)
                data += _lrv(vertex.right)
                data.append(vertex.data)
            return data 
        return _lrv(self.root)
    
    def search(self, data):
        if self.root.data == data:
            return self.root
        def find_vertex(vertex, data):
            if vertex:
                if vertex.data:
                    if vertex.data == data:
                        return vertex
                    elif data < vertex.data:
                        return find_vertex(vertex.left, data)
                    else:
                        return find_vertex(vertex.right, data)
        return find_vertex(self.root, data)


def setup(root):
    embed = tk.Frame(root, width = 1200, height = 800)
    embed.grid(columnspan = (600), rowspan = 500)
    embed.pack(side = LEFT)
    os.environ['SDL_WINDOWID'] = str(embed.winfo_id())
    os.environ['SDL_VIDEODRIVER'] = 'windib'
    pygame.init()
    pygame.font.init()
    window = pygame.display.set_mode(((1100,800)))
    window.set_alpha(None)
    return window

def draw_board(window, root, tree, coordinates):
    window.fill(pygame.Color(50, 235, 50))
    if tree.lenght > 0:
        for data in tree.lvr():
            x, y = calculate_coordinates(tree, data, coordinates)
            draw_node(window, x, y, data, tree, coordinates)
    
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONUP:
            None
        if event.type == pygame.QUIT:
            pygame.quit()
    pygame.display.update()
    root.update()

def draw_node(window, x, y, data, tree, coordinates):
    pygame.draw.circle(window, pygame.Color(255, 255, 255), (x, y), 30)
    if not tree.if_root(data):
        start_x, start_y = coordinates[tree.find_parent(data)]
        if tree.wich_child(data) == 'right':
            pygame.draw.line(window, pygame.Color(0, 0, 0), (start_x + 30, start_y), (x-30, y))
        else:
            pygame.draw.line(window, pygame.Color(0, 0, 0), (start_x-30, start_y), (x+30, y))
        font = pygame.font.SysFont('Comic Sans MS', 30)
        textsurface = font.render(str(data), False, (0, 0, 0))
        window.blit(textsurface, (x - offset(data), y-20))

def calculate_coordinates(self, tree, data, coordinates):
    level = tree.find_leve(data)
    y = 70 * (level + 1)
    if tree.if_root(data):
        x = 550
    else:
        x = coordinates[tree.find_parent(data)][0]
        if tree.wich_child(data) == 'right':
            if level < 4:
                x += 100 * (4 - level)
            else:
                x += 100
        else:
            if level < 4:
                x -= 100 * (4 - level)
            else:
                x -= 100
    coordinates[data] = (x, y)        
    return (x, y)

def offset(k):
    if k > 0:
        return (int(math.log10(k)) + 1) * 7.5
    elif k == 0:
        return 10
    else:
        return (int(math.log10(-k)) + 2) * 6

def GUI(tree):
    root = tk.Tk()
    root.title('Binary Tree')
    root.option_add('*font', 'Calibri 22')
    window = setup(root)
    bInsert = Button(text = 'Add',command=(lambda:popupInput('Add', tree)))
    bInsert.pack()
    bRemove = Button(text = 'Remove', command=(lambda:popupInput('Remove', tree)))
    bRemove.pack()
    bSearch = Button(text = 'Search', command=(lambda:popupInput('Search', tree)))
    bSearch.pack()
    bInorder = Button(text = 'VLR', command=(lambda:popup('VLR', tree)))
    bInorder.pack()
    bPreorder = Button(text = 'LVR', command=(lambda:popup('LVR', tree)))
    bPreorder.pack()
    bPostorder = Button(text = 'LRV', command=(lambda:popup('LRV', tree)))
    bPostorder.pack()
    return root, window

def popupInput(t, tree):
    data = simpledialog.askinteger('Wprowadz dane',t, minvalue=0, maxvalue=999)
    if t == 'Add':
        if tree.search(data):
            messagebox.showerror('Error', 'Istnieje juz taki wezel')
        else:
            tree.add(str(data))
            messagebox.showinfo('Sukces','Poprawnie dodano wezel')
    elif t == 'Remove':
        if tree.search(data):
            tree.usun(str(data))
            messagebox.showinfo('Sukces','Poprawnie usunieto wezel')
        else:
            messagebox.showerror('Error', 'Nie ma takiego wezla')
    elif t == 'Search':
        if tree.search(str(data)):
            messagebox.showinfo('Sukces','Wezel znajduje sie w drzewie')

        else:
            messagebox.showerror('Error', 'Nie ma takiego wezla')

def popup(t, tree):
    if t == 'VLR':
        messagebox.showinfo('VLR', str(tree.vlr()))
    elif t == 'LVR':
        messagebox.showinfo('LVR', str(tree.lvr()))
    elif t == 'LRV':
        messagebox.showinfo('LRV', str(tree.lrv()))

s = BinaryTree()
root, window = GUI(s)
coordinates = dict()

while True:
    draw_board(window, root, s, coordinates)






            

                
        

        