import random

class Box():
    def __init__(self, data=None):
        self.data = data 
        self.next = None 
        self.random = None 

class List():
    def __init__(self):
        self.head = Box()
        self.connections = {}

    def add(self, data):
        if self.head.data == None:
            self.head.data = data 
        else: 
            counter = self.head 
            i = 0
            while counter.next != None:
                counter = counter.next
            counter.next = Box(data)
    
    def set_random(self):
        if self.head:
            counter_one = self.head
            counter_two = self.head
            while counter_one.next != None:
                if counter_one != counter_two and random.randint(1, 7) % 3 and counter_two:
                    counter_one.random = counter_two
                    counter_one = counter_one.next
                    add_to_dict(self.connections, counter_one.data, counter_two.data)
                counter_two = counter_two.next
                if counter_two == None:
                    counter_two = self.head
            counter_one.random = self.head
    
    def display_list(self):
        if self.head.data == None:
            print('list is empty')
        else:
            counter = self.head
            while counter.next != None:
                print(counter.data)
                counter = counter.next
            print(counter.data)
    
    def display_random(self):
        if self.head.data == None:
            print('list is empty')
        else:
            display_dict(self.connections)
    
    def lenght_list(self):
        lenght = 0
        if self.head.data != None:
            counter = self.head
            lenght = lenght + 1
            while counter.next != None:
                counter = counter.next
                lenght = lenght + 1
        return lenght
    
    def clone_list(self):
        new = List()
        counter = self.head
        while counter.next != None:
            new.add(counter.data)
            counter = counter.next 
        new.add(counter.data)
        counter = self.head
        new_counter = new.head
        i = 0
        while counter.next != None:
            new_counter.random = counter.random
            add_to_dict(new.connections, new_counter, counter.random.data)
            counter = counter.next
            new_counter = new_counter.next
        return new 

def add_to_dict(dict, key, value):
    if key in dict:
        dict[key] = dict[key] + str(value)
    else:
        dict[key] = str(value)

def display_dict(dict):
    for x in dict:
        print(x, ': ', dict[x])

def compare_list(listA, listB):
    temp_one = listA.head
    temp_two = listB.head

    if temp_one == None and temp_two == None:
        return True

    while temp_one != None and temp_two != None:
        if temp_one.data == temp_two.data:
            temp_one = temp_one.next 
            temp_two = temp_two.next 
        else:
            return False 
    return True

list = List()
for i in range(10):
    list.add(i)
 