import random

class HashMap:
    def __init__(self, size=10):
        self.size = size
        self.table = [None] * self.size 

    def key_transformation(self, key):
        hash = 0
        for x in str(key):
            hash = hash + ord(x)
        return hash % self.size 
    
    def add(self, key, value):
        key_hash = self.key_transformation(key)
        key_value = [key, value]

        if self.table(key_hash) is None:
            self.table[key_hash] = list([key_value])
            return 
        else:
            for pair in self.table[key_hash]:
                if pair[0] == key:
                    pair[1] = value
                    return 
                self.table[key_hash].append(key_value)
                return 
    
    def get(self, key):
        key_hash = self.key_transformation(key)
        if self.table[key_hash] is not None:
            for para in self.table[key_hash]:
                if pair[0] == key:
                    return pair[1]
        return None 
    
    def delete(self, key):
        key_hash = self.key_transformation(key)
        if self.table[key_hash] is None:
            return False
        for i in range(0, len(self.table[key_hash])):
            if self.table[key_hash][i][0] == key:
                self.table[key_hash].pop(i)
                return True
    
    def display(self):
        for item in self.table:
            if item:
                for x in item:
                    print(x[0], ' : ', x[1])

def add_book(book_list, keys):
    book = HashMap(len(key))
    while True:
        print('Tytul ksiazki: ')
        title = input()
        if book_exist(book_list, title):
            print('Book ', title, 'exists')
        else:
            book.add('Title', title)
            break
    
    for key in keys:
        if key != 'Title':
            print('Give{0}'.format(key))
            data = input()
            if data.isdigit():
                data = int(data)
            book.add(key, data)
    book_list.append(book)

def book_exist(book_list, title):
    for book in book_list:
        if title == book.get('Title'):
            return True
    return False

def delete_book(book_list):
    print('Title: ')
    title = input()
    if not book_exist(book_list, title):
        print('Book ', title, 'does not exist')
    else:
        for book in book_list:
            if title == book.get('Title'):
                book_list.remove(book)
                print('Removed')
                break

def cheapest(book_list):
    index = 0
    cheapest = book_list[0].get('Price')
    for i in range(1, len(book_list)):
        if book_list[i].get('Price') < cheapest:
            cheapest = book_list[i].get('Price')
            index = i
    return index

def display(book_list, keys):
    i = 0 
    for book in book_list:
        print('Book number ', i)
        book.display()
        print(' ')
        i = i + 1

def sort_list(book_list, keys):
    print('Criterion?')
    for i in range(len(keys)):
        print('{0}.{1}'.format(i, keys[i]))
    choice = int(input())
    book_list = sort(book_list, keys[choice])

def sort(list, choice):
    if len(list) <= 1:
        return list
    else:
        left, right = split(list)
        return merge(sort(left, choice), sort(right, choice), choice)

def split(list):
    return list[:int(len(list)/2)], list[int(len(list)/2):]

def merge(left, right, choice):
    if len(left) == 0:
        return right
    if len(right) == 0:
        return left
    
    index_left = index_right = 0
    merge = []
    lenght = len(left) + len(right)
    while len(merge) < lenght:
        if left[index_left].get(choice) <= right[index_right].get(choice):
            merge.append(left[index_left])
            index_left += 1
        else:
            merge.append(right[index_right])
            index_right = index_right + 1
        
        if index_right == len(right):
            merge = merge + left[index_left:]
            break
        elif index_left == len(left):
            merge = merge + right[index_right:]
            break
    return merge

def menu(book_list, keys):
    while True:
        print('1.Dodaj ksiazke do biblioteki')
        print('2. Wyswietl ksiazki')
        print('3. Posortuj ksiazki wedlug klucza')
        print('4. Usun ksiazke')
        print('5. Pokaz najtansza')
        choice = int(input())
        if choice == 1:
            add_book(book_list, keys)
        if choice == 2:
            display(book_list, keys)
        if choice == 3:
            sort_list(book_list, keys)
        if choice == 4:
            delete_book(lista_ksiazek)
        if choice == 5:
            book_list[cheapest(book_list)].display()

book_list = []
keys = ['Title', 'Author', 'Year of publishment', 'Price', 'Number of pages']
menu(book_list, keys)

