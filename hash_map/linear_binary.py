def binary_search(list, k):
    left = 0
    right = len(list)
    while left < right:
        mid = int((right+left)/2)
        if k == list[mid]:
            return mid
        else:
            if k < list[mid]:
                right = mid - 1
            else:
                left = mid + 1
    return -1

def recursion_binary_search(list, left, right, k):
    if right < left:
        return -1
    mid = int((left+right)/2)
    if k == list[mid]:
        return mid
    else:
        if k < list[mid]:
            return recursion_binary_search(list, left, mid - 1, k)
        else:
            return recursion_binary_search(list, mid + 1, right, k)

def linear_search(list, k):
    for i in range(len(list)):
        if k == list[i]:
            return i 
    return -1
