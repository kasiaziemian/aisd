class Graph():
    def __init__(self):
        self.neighbour_dict = dict()
    
    def add_vertex(self, vertex):
        if vertex in self.neighbour_dict:
            print('added')
        else:
            self.neighbour_dict[vertex] = []
    
    def add_edge(self, start, end):
        if start in self.neighbour_dict and end in self.neighbour_dict:
            self.neighbour_dict[start] += [end]
    
    def bfs(self, start):
        def _bfs(dict, vertex):
            visited, queue = set(), [vertex]
            visited.add(vertex)
            while queue:
                vertex = queue.pop()
                for neighbour in dict[vertex]:
                    if neighbour not in visited:
                        visited.add(neighbour)
                        queue.append(neighbour)
            return visited
        return _bfs(self.neighbour_dict, start)
    
    def dfs(self, start):
        def _dfs(dict, vertex, visited=[]):
            if not visited or vertex not in visited:
                visited.append(vertex)
                for neighbour in dict[vertex]:
                    _dfs(dict, neighbour, visited)
            return visited
        return _dfs(self.neighbour_dict, start)


g = Graph()
g.add_vertex('A')
g.add_vertex('B')
g.add_vertex('C')
g.add_vertex('D')
g.add_vertex('E')
g.add_vertex('F')
g.add_vertex('G')
g.add_vertex('H')

g.add_edge('A', 'D')
g.add_edge('A', 'C')
g.add_edge('A', 'E')
g.add_edge('B', 'D')
g.add_edge('C', 'F')
g.add_edge('E', 'B')
g.add_edge('F', 'E')
g.add_edge('D', 'G')
g.add_edge('D', 'F')
g.add_edge('G', 'F')
g.add_edge('H', 'H')

print(g.dfs('A'))
print(g.bfs('A'))
