import random 
import math 
import pygame
from pygame.locals import *
import time

width = 1400
height = 800
total_cities = 100
cities = []

for i in range(total_cities):
    cities.append((random.randint(30, width), random.randint(40, height)))

min = cities
generation = 0

def setup():
    pygame.init()
    pygame.display.set_caption('salesman')
    window = pygame.display.set_mode(((1400, 800)))
    window.set_alpha(None)
    window.fill(pygame.Color(0, 0, 0))
    return window

def draw(window, cities, min, x, y):
    global generation
    window.fill(pygame.Color(0, 0, 0))
    white = pygame.Color(255, 255, 255)
    for i in range(len(min)):
        pygame.draw.circle(window, white, min[i], 10)
        if i != len(min) -1:
            pygame.draw.line(window, white, min[i], min[i + 1])
        else:
            pygame.draw.line(window, white, min[len(min) - 1], min[0])
    
    min = find_new_min(min, cities)
    text(window, white, 'current distance: ' + str("%.2f" % calc_distance(min)), 5, 5)
    text(window, white, 'current generation: ' + str(generation), 5, 35)
    generation += 1
    swap(x, y, cities)
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONUP:
            None
        if event.type == pygame.QUIT:
            pygame.quit()
    pygame.display.update()


def text(window, color, text, x, y):
    pygame.font.init()
    my_font = pygame.font.SysFont('Calibri MS', 30)
    text_surface = my_font.render(text, False, color)
    window.blit(text_surface, (x, y))

def swap(i, j, board):
    temp = board[i]
    board[i] = board[j]
    board[j] = temp

def calc_distance(cities):
    sum = 0
    for i in range(len(cities) - 1):
        sum += math.sqrt((cities[i][0]-cities[i+1][0])**2 + (cities[i][1]-cities[i+1][1])**2)
    sum += math.sqrt((cities[0][0]-cities[len(cities)-1][0])**2 + (cities[0][1]-cities[len(cities)-1][1])**2)
    return sum

def find_new_min(min, cities):
    if calc_distance(cities) < calc_distance(min):
        return cities
    return min 

def recurse(y, num):
    if (num > 1):
        recurse(y, num - 1)
    else:
        for x in range(y):
            draw(window, cities, min, x, y)

window = setup()
while True:
    for y in range(total_cities):
        recurse(y, total_cities)
 
