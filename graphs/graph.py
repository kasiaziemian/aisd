def path(graph):
    edge = []
    for vertex in graph:
        for neighbour in graph[vertex]:
            edge.append(vertex, neighbour)
    return edge

def find_path(graph, start, end, path=[]):
    if start in graph and end in graph:
        path += [start]
        if start == end:
            return path 
        for neighbour in graph[start]:
            if neighbour not in path:
                new_path = find_path(graph, neighbour, end, path)
                if new_path:
                    return path

def shortest_path(graph, start, end, path=[]):
    if start in graph and end in graph:
        path += [start]
        if start == end:
            return path
        shortest = None
        for neighbour in graph[start]:
            if neighbour not in path:
                new_path = shortest_path(graph, neighbour, end, path)
                if new_path:
                    if not shortest:
                        shortest = new_path
                    elif len(shortest) > len(new_path):
                        shortest = new_path
        return shortest

def all_paths(graph, start, end, path=[]):
    if start in graph and end in graph:
        path += [start]
        if start == end:
            return path 
        found = []
        for neighbour in graph[start]:
            if neighbour not in path:
                new_path = all_paths(graph, neighbour, end, path)
                if new_path and not new_path in found:
                    found.append(new_path)
        return found

def loop(graph, vertex):
    for neighbour in graph[vertex]:
        if neighbour == vertex:
            return True
    return False

def find_isolated_vertex(graph):
    isolate = []
    for vertex in graph:
        if not graph[vertex]:
            isolate.append(vertex)
    return isolate

def add_edge(graph, start, end):
    if start in graph:
        graph[start].append(end)
    else:
        graph[start] = [end]

def vertex_degree(graph, vertex):
    return len(graph[vertex])

def coherent(graph):
    for x in graph:
        if not graph[x]:
            return False
    return True

graph = {
    'A' : ['A', 'B', 'C'],
    'B' : ['C', 'D'],
    'C' : ['D'],
    'D' : ['C'],
    'E' : ['F'],
    'F' : ['C'],
    'G' : [],
    'P' : []
}
