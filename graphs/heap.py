import random

class Heap():
    def __init__(self, size = 10):
        self.size = size
        self.heap = [0] * self.size
        self.actl_pos = -1
    
    def insert(self, item):
        if self.overflow():
            print('overflow')
            return 
        
        self.actl_pos = self.actl_pos + 1
        self.heap[self.actl_pos] = item
        self.repair_top(self.actl_pos)
    
    def repair_top(self, pos):
        parent_pos = int((pos-1)/2)

        while parent_pos >= 0 and self.heap[parent_pos] < self.heap[pos]:
            temp = self.heap[pos]
            self.heap[pos] = self.heap[parent_pos]
            self.heap[parent_pos] = temp
            pos = parent_pos
            parent_pos = (int)((pos - 1) / 2)
    
    def heap_sort(self):
        for i in range(0, self.actl_pos + 1):
            temp = self.heap[0]
            print(f"{temp}")
            self.heap[0] = self.heap[self.actl_pos - i]
            self.heap[self.actl_pos - i] = temp
            self.repair_down(0, self.actl_pos - i - 1)
    
    def repair_down(self, pos, end):
        while pos <= end:
            left_child = 2 * pos + 1
            right_child = 2 * pos + 2

            if left_child < end:
                change_index = None

                if right_child > end:
                    change_index = left_child
                
                else:
                    if self.heap[left_child] > self.heap[right_child]:
                        change_index = left_child
                    else:
                        change_index = right_child
                    
                    if self.heap[pos] < self.heap[change_index]:
                        temp = self.heap[pos]
                        self.heap[pos] = self.heap[change_index]
                        self.heap[change_index] = temp
                    else:
                        break

                    pos = change_index
            else:
                break 
    
    def overflow(self):
        if self.actl_pos == self.size:
            return True
        return False

heap = Heap()

for i in range(10):
    x = random.randint(0, 100)
    print('Insert: ', x)
    heap.insert(x)

heap.heap_sort()
